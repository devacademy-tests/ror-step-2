###################
Live Version
###################

http://mariovlad.com/ror/



###################
Git
###################

https://gitlab.com/devacademy-tests/ror-step-2



###################
Rquirements
###################

PHP 5.6+
MySQL 5.5+
Apache 2.2+



###################
Installation
###################

Import /ror.sql into a MySQL server and configure the credentials in /application/config/database.php