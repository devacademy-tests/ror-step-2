-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 21, 2017 at 01:01 AM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ror`
--
CREATE DATABASE IF NOT EXISTS `ror` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ror`;

-- --------------------------------------------------------

--
-- Table structure for table `lobby`
--

CREATE TABLE IF NOT EXISTS `lobby` (
  `id` mediumint(9) NOT NULL,
  `group_id` tinyint(4) NOT NULL,
  `user_1` mediumint(9) NOT NULL,
  `user_2` mediumint(9) NOT NULL,
  `user_3` mediumint(9) NOT NULL,
  PRIMARY KEY (`id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `username` varchar(12) NOT NULL,
  `match_wins` mediumint(9) NOT NULL DEFAULT '0',
  `campionship_wins` mediumint(9) NOT NULL DEFAULT '0',
  `logged_in` tinyint(4) NOT NULL DEFAULT '0',
  `last_active` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='table used to store usernames and scores' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `match_wins`, `campionship_wins`, `logged_in`, `last_active`) VALUES
(1, 'rrr', 0, 0, 1, '2017-10-21 01:45:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
