<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{
    
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    
    private $_username;
    
    public function __construct()
    {
        parent::__construct();
        
        // load helpers
        $this->load->helper(array(
            'form',
            'url',
            'menu'
        ));
        $this->load->library('session');
        
        $this->load->model('main_model');
        if ($this->session->has_userdata('username'))
        {
            $this->_username = $this->session->userdata('username');
            $this->main_model->save_activity($this->_username);
        }
        else
        {
            $this->_username = FALSE;
        }
    }
    
    public function index()
    {
        $this->load->library('form_validation');
        $this->load->helper('security');
        
        $this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean|strip_tags|min_length[3]|max_length[12]|callback_validate_member');
        $this->form_validation->set_message('validate_member', 'The user is already active!');
        
        if ($this->form_validation->run() == FALSE)
		{
            
            $data_view['metatitle'] = 'Rock Paper Scissors Lizard Spock';
            $data_view['metadesc']  = 'Rock Paper Scissors Lizard Spock CodeIgniter implementation';
            
            $data_view['username'] = $this->_username;
            
            $this->load->view('header', $data_view);
            $this->load->view('home');
            $this->load->view('footer');
        }
		else
		{
            $this->_username = $this->input->post('username', TRUE);
            
            $users = $this->main_model->save_user($this->_username);
            
            $this->session->set_userdata('username', $this->_username);
            
            redirect('lobby/');
        }
    }
    
    public function lobby()
    {
        
        $data_view['username'] = $this->_username;
        if ($data_view['username'] === FALSE)
		{
            redirect('/');
        }
        $data_view['metatitle'] = 'Lobby | Rock Paper Scissors Lizard Spock';
        $data_view['metadesc']  = 'Loby for the Rock Paper Scissors Lizard Spock CodeIgniter implementation';
        
        
        $this->main_model->enter_lobby($this->_username);
        
        
        $this->load->view('header', $data_view);
        $this->load->view('lobby', $data_view);
        $this->load->view('footer');
    }
    
    public function leaderboard()
    {
        $data_view['username']  = $this->_username;
        $data_view['metatitle'] = 'Lobby | Rock Paper Scissors Lizard Spock';
        $data_view['metadesc']  = 'Loby for the Rock Paper Scissors Lizard Spock CodeIgniter implementation';
        
        
        $data_view['users'] = $this->main_model->get_leaderboard();
        
        
        $this->load->view('header', $data_view);
        $this->load->view('leaderboard', $data_view);
        $this->load->view('footer');
    }
    
    public function about()
    {
        $data_view['metatitle'] = 'About | Rock Paper Scissors Lizard Spock';
        $data_view['metadesc']  = 'About the Rock Paper Scissors Lizard Spock CodeIgniter implementation';
        
        $data_view['username'] = $this->_username;
        
        $this->load->view('header', $data_view);
        $this->load->view('about', $data_view);
        $this->load->view('footer');
    }
    
    public function logout()
    {
        
        if ($this->_username)
		{
            $this->session->unset_userdata('username');
            $this->main_model->logout($this->_username);
        }
        redirect('/');
    }
    
    function validate_member($username)
    {
        if ($this->main_model->validate_member($username))
		{
            return FALSE;
        } else
		{
            return TRUE;
        }
    }
}
