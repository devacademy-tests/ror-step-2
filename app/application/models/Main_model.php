<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model
{
    /**
     * @vars
     */
    private $_users_table;
    private $_lobby_table;
    
    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        // define primary table
        $this->_users_table = 'users';
        $this->_lobby_table = 'lobby';
        $this->load->database();
    }
    
    public function save_user($username)
    {
        
        $data = array(
            'username' => $username,
            'logged_in' => 1
        );
        $this->db->where('username', $username);
        $this->db->from($this->_users_table);
        if ($this->db->count_all_results() === 0)
        {
            $this->db->insert($this->_users_table, $data);
        }
    }
    
    public function save_activity($username)
    {
        
        $data = array(
            'username' => $username,
            'last_active' => date('Y-m-d H:i:s')
        );
        $this->db->where('username', $username);
        $this->db->from($this->_users_table);
        $this->db->update($this->_users_table, $data);
    }
    
    public function validate_member($username)
    {
        $sess_exp = '-' . $this->config->item('sess_expiration') . ' seconds';
        $this->db->where('username', $username);
        $this->db->where('last_active >', date('Y-m-d H:i:s', strtotime($sess_exp)));
        $this->db->from($this->_users_table);
        return $this->db->count_all_results();
    }
    
    public function get_leaderboard()
    {
        $this->db->select('username, match_wins, campionship_wins');
        $this->db->order_by("campionship_wins", "desc");
        $this->db->order_by("match_wins", "desc");
        $this->db->from($this->_users_table);
        return $this->db->get()->result_array();
    }
    
    
    public function enter_lobby($username)
    {
    	// Get the current user userid
        $this->db->select('id');
        $this->db->where('username', $username);
        $this->db->from($this->_users_table);
        $user_id = $this->db->get()->row();        
        $user_id = $user_id->id; // TODO: this might end up being FALSE
        
        // Check if the user is already inside a lobby
        $this->db->where('user_1', $user_id, null, false);
        $this->db->or_where('user_2', $user_id, null, false);
        $this->db->or_where('user_3', $user_id, null, false);
        $this->db->from($this->_lobby_table);
        $this->db->limit(1);
        
        if ($this->db->count_all_results() == 0)
        {
            $this->db->select('id, group_id');
            $this->db->where('user_1', '', null, false);
            $this->db->or_where('user_2', '', null, false);
            $this->db->or_where('user_3', '', null, false);
            $this->db->from($this->_lobby_table);
            $this->db->limit(1);
            
            if ($this->db->count_all_results() == 0)
            {
                //check if we have free groups in the lobby
                return;
            }
            
            $free_row = $this->db->get()->row();
            
            
            $data = array(
                'user_1' => $user_id
            );
            
            $this->db->where('user_1', '');
            $query = $this->db->update($this->_lobby_table, $data);
            
            if ($query->num_rows())
                return;
            
            $data = array(
                'user_2' => $user_id
            );
            
            $this->db->where('user_2', '');
            $query = $this->db->update($this->_lobby_table, $data);
            
            if ($query->num_rows())
                return;
            
            $data = array(
                'user_3' => $user_id
            );
            
            $this->db->where('user_3', '');
            $query = $this->db->update($this->_lobby_table, $data);
            
            if ($query->num_rows())
                return;
        }
        
        
    }
    
    public function logout($username)
    {
        $data = array(
            'logged_in' => 0
            //'last_active' => 0           
        );
        
        $this->db->where('username', $username);
        $this->db->update($this->_users_table, $data);
    }
    
    public function update_entry()
    {
        $this->title   = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date    = time();
        
        $this->db->update('entries', $this, array(
            'id' => $_POST['id']
        ));
    }
    
}