<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">  
  
	<div class="row">

        <div class="col-xs-4"></div>
        <div class="col-xs-4">			
            
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Username</th>
        <th>Match Wins</th>
        <th>Championship Wins</th>
      </tr>
    </thead>
    <tbody>
	<? foreach($users as $key=>$user) { ?>
      <tr>
      	<td><?=$key+1?></td>
        <td><?=$user['username']?></td>
        <td><?=$user['match_wins']?></td>
        <td><?=$user['campionship_wins']?></td>
      </tr>
  	<? } ?>
    </tbody>
  </table>
        </div>
        <div class="col-xs-4"></div>
	</div>

</div><!-- /.container -->